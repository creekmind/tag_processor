# Tag processor 0.3.0 #

Application allows processing string like **"I like ${data_container__something}"** by replacing tags with information
passed with a special data container.

### Simple tag: ###

```
#!python


data = DataContainer()
data.animal = 'dogs'
input_string = "I like ${animal}"
processor = TagProcessor(data)
print processor.execute(input_string)
```


**Result is:** I like dogs


### Complex tag: ###


```
#!python

data = DataContainer()
data.animal = {
    'like': 'cats',
    'dislike': 'dogs'
}
input_string = "I like ${animal__like} and don't like ${animal__dislike}"
processor = TagProcessor(data)
print processor.execute(input_string)
```


**Result is:** I like cats and don't like cats


### More complex tag: ###


```
#!python

def dateformat(value, date_format):
    return value.strftime(date_format)

data.animal = 'dogs'
data.birthday = datetime.datetime.now()
data.dateformat = dateformat
input_string = "I haven't liked ${animal} since ${data.birthday[dateformat=%m.%d.%Y]}"
processor = TagProcessor(data)
print processor.execute(input_string)
```


**Result is:** "I haven't liked dogs since 01.05.2016"

### Some more complex tag: ###


```
#!python

def first(value, params):
    return value[0]

def upper(value, params):
    return value.upper()

data.first = first
data.animals = [{
    'name': 'cats'
}, {
    'name': 'dogs'
}]
input_string = "What do you think about ${animals[first]__name[upper]}?"
```



**Result is:** What do you think about CATS?



### Disjunction tag: ###


```
#!python

data.default = "empty-str"
input_string = "If result variable is None: ${${result}|${default}}"
```


**Result is:** If result variable is None: empty-str


### Ternary tag: ###


```
#!python

data.museum = 'museums'
data.pub = 'pubs'
data.is_first_time = True
input_string = "You should visit ${is_first_time?${museum}:${pub}}"
```


**Result is:** You should visit museums


### Multiple parameters: ###


```
#!python

data = DataContainer()
data.update({
    'vehicle_1': 'Cart',
    'driver_1': 'Meatwad'
})
input_string = u"${[join=/,${vehicle_1},${driver_1}]}"
```


**Result is:** Cart/Meatwad